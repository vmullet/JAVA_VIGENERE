package model;

/**
 * Classe contenant les m�thodes de cryptage et de d�cryptage de Vigenere
 * @author Valentin Mullet
 *
 */
public final class Vigenere {

	static int ASCII_DELTA=65;
	static int NUMBER_OF_LETTERS=26;
	
	/**
	 * M�thode permettant le cryptage d'un texte et l'export de cette version crypt�e
	 * @param key Cl� utilis�e pour crypter le texte
	 * @param text Texte � crypter
	 * @param cryptedText Chemin du fichier pour l'export de la version crypt�e
	 */
	public static void crypter(String key,String text,String cryptedTextPath) {
		
		String result="";
		int letterCount=0;
		text=text.toUpperCase();
		
		for (int cnt=0;cnt<text.length();cnt++) {
		
			if (Character.isLetter(text.charAt(cnt))) {
				
				String keyChar=key.charAt(letterCount%key.length())+""; //On r�cup�re la lettre de la cl� � associer avec la lettre du texte
				
				int columnAscii=(int)text.charAt(cnt)-ASCII_DELTA; //R�cup�ration du num�ro de la ligne sur la table de Vigen�re
				int lineAscii=(int)keyChar.charAt(0)-ASCII_DELTA; //R�cup�ration du num�ro de la colonne sur la table de Vigen�re
				
				int cryptCharaAscii=(lineAscii+columnAscii)%NUMBER_OF_LETTERS; //Code ASCII du caract�re crypt�
				String cryptchara=(char)(cryptCharaAscii+ASCII_DELTA)+""; //Conversion du code ASCII en caract�re
					
				letterCount++;
				
				result+=cryptchara;
				
			}
			
			else {
				
				result+=text.charAt(cnt); //S'il ne s'agit pas d'une lettre
			}
			
		}
		
		FileWriter f =new FileWriter(cryptedTextPath);
		
		f.Write(result);
		
	}
	
	/**
	 * M�thode permettant le d�cryptage d'un texte et l'export de cette version d�crypt�e
	 * @param key Cl� utilis�e pour d�crypter le texte
	 * @param text Texte � d�crypter
	 * @param decryptedTextPath Chemin du fichier d'export pour la version d�crypt�e
	 */
	public static void decrypter(String key,String text,String decryptedTextPath) {
		
		String result="";
		int letterCount=0;
		text=text.toUpperCase();
		
		for (int cnt=0;cnt<text.length();cnt++) {
			
			if (Character.isLetter(text.charAt(cnt))) {
				
				String keyChar=key.charAt(letterCount%key.length())+""; //On r�cup�re la lettre de la cl� � associer avec la lettre du texte
				
				int lineAscii=(int)keyChar.charAt(0)-ASCII_DELTA; //R�cup�ration du num�ro de la colonne sur la table de Vigen�re
				
				int cryptNb=(int)text.charAt(cnt)-ASCII_DELTA; //Recuperation du num�ro de la lettre crypt�e
				
				int columnNb = (cryptNb-lineAscii)%NUMBER_OF_LETTERS; //Recuperation du bon numero de colonne dans la table de Vigenere
				if (columnNb<0)
					columnNb+=NUMBER_OF_LETTERS;
				
				char decryptChar = (char)(columnNb+ASCII_DELTA); //Caract�re d�crypt�e
				
				letterCount++;
				result+=decryptChar;
				
			}
			else {
				result+=text.charAt(cnt);
			}
			
		}
		
		FileWriter f = new FileWriter(decryptedTextPath);
		
		f.Write(result);
		
	}
	
	
	
}
