package model;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Classe permettant d'effectuer une cryptanalyse de Vigen�re
 * @author Valentin Mullet
 *
 */
public class Cryptanalyse {

	private String fileName; //Chemin d'acc�s du fichier crypt�
	private String rawFileContent; //Contenu brut du fichier crypt� (caract�res,espaces,etc...)
	private String charaFileContent; //Caract�res de type lettres  contenu dans le fichier crypt� (tous en majuscule)
	private int keySize; // Taille de la cl�
	private String keyValue; // Valeur de la cl� utilis�e pour crypter le fichier
	private int ASCII_DELTA=65; // Code ASCII � ajouter ou d�duire selon les calculs (Il s'agit de celui de la lettre "A")
	private int ASCII_COMMON_LETTER=69; // Code SSCII de la lettre la plus commune dans la langue du texte crypt� ("E" pour l'anglais)
	private int NUMBER_OF_LETTERS = 26; // Nombre de lettres dans l'alphabet
	
	public Cryptanalyse(String pfilename) {
		
		rawFileContent="";
		charaFileContent="";
		fileName = pfilename;
		keySize = -1;
		keyValue = "-1";
		loadFileContent();
		
	}
	
	
	public int getKeySize() {
		if (keySize==-1)
			System.out.println("Vous n'avez pas lanc� la m�thode de calcul de la taille de la cl�");
		return keySize;
	}
	
	public String getFileName() {
		return fileName;
	}


	public String getKeyValue() {
		if (keyValue=="-1")
			System.out.println("Vous n'avez pas lanc� la m�thode de calcul de la valeur de la cl�");
		return keyValue;
	}
	
	public String getRawFileContent() {
		return rawFileContent;
	}


		public String getCharaFileContent() {
		return charaFileContent;
	}

	/**
	 * M�thode permettant de charger le contenu du fichier brut et l'ensemble des caract�res dans deux variables distinctes
	 */
	private void loadFileContent() {
		
		try (BufferedReader br = new BufferedReader(new FileReader(fileName)))
		{

			String sCurrentLine="";

			while ((sCurrentLine=br.readLine()) != null) {
				sCurrentLine = sCurrentLine.toUpperCase().toString();
				rawFileContent += sCurrentLine+"\n" ; //Ajout des retours � la ligne
				
				for (int i=0;i<sCurrentLine.length();i++) {
					
					if (Character.isLetter(sCurrentLine.charAt(i))) {
							charaFileContent+=sCurrentLine.charAt(i);
						
					}
						
				}
					
			}
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * M�thode retournant le nombre de caract�re de type lettre
	 * @return Le nombre de caract�res de type lettre
	 */
	public int getNbCharacter() {
		
		return charaFileContent.length();
	}
	
	/**
	 * M�thode retournant le nombre de caract�re de type lettre
	 * dans un texte pass� en param�tre
	 * @param text Texte dont il faut compter les lettres
	 * @return Nombre de caract�re de type lettre
	 */
	public int getNbCharacter(String text) {
		
		int nbChara = 0;
		
		for (int i=0;i<text.length();i++) {
			
			if (Character.isLetter(text.charAt(i))) {
				nbChara++;
			}
			
		}
		
		return nbChara;
		
	}
	
	/**
	 * M�thode retournant l'indice de coincidence de l'ensemble du texte
	 * @return Indice de coincidence de l'ensemble du texte
	 */
	public double getRAWIC() {
		
		double ic =0;
		
		int textCharaSize = this.getNbCharacter(); //Nombre de lettres du texte
		
		for (int alphabPos =0;alphabPos<NUMBER_OF_LETTERS;alphabPos++) {
			
			String charaToTest = (char) (alphabPos+ASCII_DELTA)+""; //Caract�re de l'alphabet � tester
			
			int occurence = this.getOccurenceChara(charaToTest); // Nombre d'occurence de ce caract�re
			
			System.out.println(charaToTest.toUpperCase() +" --> " + occurence);
			
			ic+=((double)occurence/(double)textCharaSize)
					*((double)(occurence-1)/(double)(textCharaSize-1)); //Calcul de l'IC
			
		}
		
		return ic; //Renvoi de l'IC apr�s tous les caract�res test�s
	}
	
	/**
	 * M�thode permettant de calculer l'IC d'un texte pass� en param�tre
	 * @param text Texte dont on veut calculer l'IC
	 * @return IC du texte pass� en param�tre
	 */
	public double getRAWIC(String text) {
		
		double ic =0;
		
		int textCharaSize = this.getNbCharacter(text); // Nombre de lettres du texte
		
		for (int alphabPos =0;alphabPos<NUMBER_OF_LETTERS;alphabPos++) {
			
			String charaTest = (char) (alphabPos+ASCII_DELTA)+""; // Caract�re de l'alphabet � tester
			
			int occurence = this.getOccurenceChara(text,charaTest); // Nombre d'occurence du caract�re test�
			
			System.out.println(charaTest.toUpperCase() +" --> " + occurence);
			
			ic+=((double)occurence/(double)textCharaSize)
					*((double)(occurence-1)/(double)(textCharaSize-1)); //Calcul de l'IC
			
		}
		
		return ic; // Renvoi de l'IC apr�s tous les caract�res test�s
	}
	
	/**
	 * M�thode retournant un sous-texte en partant du d�but (indice 0) 
	 * avec un pas pass� en param�tre
	 * @param jumpValue Pas utilis� pour d�terminer le sous-texte
	 * @return Le sous-texte d�termin� avec ce pas
	 */
	public String getFirstSubText(int jumpValue) {
		
		String subText = "";
		
		for (int charaPos=0;charaPos<charaFileContent.length();charaPos+=jumpValue) {
			
			subText+=charaFileContent.charAt(charaPos); //Le caract�re est dans le sous-texte
				
		}
		return subText; //Sous-texte renvoy�
		
	}
	
	
	/**
	 * M�thode permettant de retourner l'ensemble des sous-textes 
	 * du texte en attribut avec un pas sp�cifi�
	 * @param jumpValue Saut pass� en param�tre
	 * @return Ensemble des sous-textes dans un tableau de la taille du saut
	 */
	public String[] getAllSubTexts(int jumpValue) {
		
		String[] subTexts = new String[jumpValue]; // Ex : Pour un saut de 9 , il y a 9 sous-textes
		String subText ="";
		
		for (int startSubPos=0;startSubPos<jumpValue;startSubPos++) { // Index de d�part pour chaque sous-texte ( entre 0 et saut-1)
			
			subText = "";
			
			for (int charaPos =startSubPos;charaPos<charaFileContent.length();charaPos+=jumpValue) { // On parcourt l'ensemble des lettres 
				//en partant du point de d�part du sous-texte
				//avec un pas �gal au saut
				
				subText+=charaFileContent.charAt(charaPos);
			
			}
			
			subTexts[startSubPos]=subText; //Affectation de chaque sous-texte
		}
		
		return subTexts;
	}
	
	
	/**
	 * Retourne l'ensemble des sous-textes 
	 * d'un texte pass� en param�tre avec un pas sp�cifi�
	 * @param monTexte Texte pass� en param�tre
	 * @param jumpValue Saut pass� en param�tre
	 * @return Ensemble des sous-textes dans un tableau de la taille du saut
	 */
	public String[] getAllSubTexts(String monTexte,int jumpValue) {
		
		String[] subTexts = new String[jumpValue]; // Ex : Pour un saut de 9 , il y a 9 sous-textes
		String subText ="";
		
		for (int startSubPos=0;startSubPos<jumpValue;startSubPos++) { // Index de d�part pour chaque sous-texte ( entre 0 et saut-1)
			
			subText = "";
			
			for (int charaPos=startSubPos;charaPos<monTexte.length();charaPos+=jumpValue) { // On parcourt l'ensemble des lettres 
				//en partant du point de d�part du sous-texte
				//avec un pas �gal au saut
				
				subText+=monTexte.charAt(charaPos);
			
			}
			
			subTexts[startSubPos]=subText; //Affectation de chaque sous-texte
		}
		
		return subTexts;
	}
	
	
	
	
	/**
	 * Retourne le nombre d'occurence d'un caract�re pass� en param�tre
	 * dans le texte qui est en attribut
	 * @param chara Caract�re dont on veut le nombre d'occurence
	 * @return Nombre d'occurence de ce caract�re
	 */
	public int getOccurenceChara(String chara) {
		
		int occurence = 0;
		
		for (int i=0;i<charaFileContent.length();i++) { 
			
				if (charaFileContent.charAt(i)==chara.toUpperCase().charAt(0)) { 
					occurence++;
				}
				
		}
		
		return occurence;
	}
	
	/**
	 * Retourne le nombre d'occurence d'un caract�re pass� en param�tre
	 * dans un texte aussi pass� en param�tre
	 * @param text Texte dans lequel on recherche le nombre d'occurence du caract�re
	 * @param chara Caract�re dont on recherche le nombre d'occurence
	 * @return Occurence du caract�re pass� en param�tre
	 */
	public int getOccurenceChara(String text,String chara) {
		
		int occurence = 0;
		
		for (int i=0;i<text.length();i++) {
			
			if (Character.isLetter(text.charAt(i))) {
				
				if (text.charAt(i)==chara.toUpperCase().charAt(0)) {
					occurence++;
				}
				
			}
			
		}
		
		return occurence;
	}
	
	
	/**
	 * M�thode permettant de calculer la taille de la cl�
	 */
	public void calculateKeySize() {
		
		double icMax =0;
		
		for (int cntJump=2;cntJump<10;cntJump++) {
			
			String subText = this.getFirstSubText(cntJump); //R�cup�ration du premier sous-texte avec un pas de cntJump
			
			double ic =this.getRAWIC(subText); //Calcul de l'IC de ce texte
				
			if (ic>icMax) {
					icMax=ic;
					keySize = cntJump; //Taille de la cl� = cntJump pour l'IC max
			}
			
		}
		
	}
	
	/**
	 * M�thode permettant de calculer la valeur de la cl�
	 */
	public void calculateKeyValue() {
		
		String theKey = "";
		String[] allSubTexts = getAllSubTexts(keySize); //R�cup�ration de tous les sous-textes avec un pas �gal � la taille de la cl�
		
		for (int allSubPos =0;allSubPos<allSubTexts.length;allSubPos++) { //Dans l'ensemble des sous-textes
			
			int maxOccurence = 0;
			String bestChara="";
			
			for (int alphabPos =0;alphabPos<NUMBER_OF_LETTERS;alphabPos++) { //Pour chaque caract�re de l'alphabet
				
				String charaToTest = (char)(alphabPos+ASCII_DELTA)+"";
				
				int occurence = getOccurenceChara(allSubTexts[allSubPos],charaToTest); //Nombre d'occurence de ce caract�re
				
				if (occurence>maxOccurence) {
					
					maxOccurence=occurence;
					
					bestChara = getCharaFromBest(charaToTest); //Calcul du caract�re dans la cl�
					//� partir du caract�re le plus courant du sous-texte
				}
					
				
			}
			
			theKey+=bestChara; //Ajout de ce caract�re dans la cl� calcul�e
			
		}
		
		keyValue = theKey;
		
	}
	
	/**
	 * M�thode permettant de calculer la valeur de la cl� en sp�cifiant la taille
	 * @param thekeySize Taille de la cl� pass�e en param�tre
	 */
	public void calculateKeyValue(int thekeySize) {
		
		String theKey = "";
		String[] allSubTexts = getAllSubTexts(thekeySize); //R�cup�ration de tous les sous-textes avec un pas �gal � la taille de la cl� en param�tre
		
		for (int allSubPos =0;allSubPos<allSubTexts.length;allSubPos++) { //Dans l'ensemble des sous-textes
			
			int maxOccurence = 0;
			String bestChara="";
			
			for (int alphabPos =0;alphabPos<26;alphabPos++) { //Pour chaque caract�re de l'alphabet
				
				String charaToTest = (char)(alphabPos+ASCII_DELTA)+"";
				
				int occurence = getOccurenceChara(allSubTexts[allSubPos],charaToTest); //Nombre d'occurence de ce caract�re
				System.out.println("Sous Texte "+(allSubPos+1)+" : "+charaToTest+"--->"+occurence);
				
				if (occurence>maxOccurence) {
					
					maxOccurence=occurence;
					bestChara = getCharaFromBest(charaToTest); //Calcul du caract�re dans la cl�
					//� partir du caract�re le plus courant du sous-texte
					
				}
					
			}
			
			theKey+=bestChara; //Ajout de ce caract�re dans la cl� calcul�e
		}
		
		keyValue = theKey;
		
	}

	/**
	 * M�thode permettant de d�terminer le caract�re de la cl�
	 * � partir du meilleur caract�re trouv�
	 * @param bestChara Meilleur caract�re du sous-texte pass� en param�tre
	 * @return Caract�re de la cl� d�termin�
	 */
	private String getCharaFromBest(String bestChara) {
		
		String finalChara ="";
		
		int alphabOrder = (bestChara.charAt(0)-ASCII_COMMON_LETTER)%NUMBER_OF_LETTERS; //Calcul de l'ordre alphab�tique du caract�re de la cl�
		
		if (alphabOrder<0) {
			alphabOrder+=NUMBER_OF_LETTERS;
		}
			
		finalChara = (char)(alphabOrder+ASCII_DELTA)+""; //Caract�re de la cl�
		
		return finalChara;
	}
			
}
