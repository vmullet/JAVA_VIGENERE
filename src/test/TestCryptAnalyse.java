package test;
import model.Cryptanalyse;
import model.Vigenere;

/**
 * Classe servant de test � la cryptanalyse de Vigenere
 * @author Valentin Mullet
 *
 */
public class TestCryptAnalyse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		for (int i=0;i<2;i++) {
			
			Cryptanalyse c1 = new Cryptanalyse("resources/boulot"+(i+1)+".txt"); //Cryptanalyse du fichier pass� en param�tre
			c1.calculateKeySize();
			c1.calculateKeyValue();
			System.out.println("Pour le fichier "+c1.getFileName()+", la taille de la cl� est : "+c1.getKeySize());
			System.out.println("Pour le fichier "+c1.getFileName()+", la valeur de la cl� est : "+c1.getKeyValue());
			
			Vigenere.decrypter(c1.getKeyValue(),c1.getRawFileContent(),"output/boulot"+(i+1)+"_decrypte.txt"); //D�cryptage du texte avec la cl�,
			// le contenu texte brut et le chemin du fichier d'export dans le dossier output
			
			
			//Test du cryptage
			
			Cryptanalyse c2 = new Cryptanalyse("output/boulot"+(i+1)+"_decrypte.txt");
			
			Vigenere.crypter(c1.getKeyValue(),c2.getRawFileContent(), "output/boulot"+(i+1)+"_crypte.txt"); //On recrypte en utilisant la cl�
			//de l'objet Cryptanalyse c1
			
		}
		
		
		
	}
	
	
}
